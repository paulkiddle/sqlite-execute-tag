import sqlDb from '../src/index.js';

test('sql', async ()=>{
	const sql = sqlDb(':memory:', { verbose: true });

	const table = sql('test_table');

	await sql`CREATE TABLE ${table} (col_1, col_2)`;
	await sql`INSERT INTO ${table} (col_1, col_2) VALUES ('row.1.1', 'row.1.2'), ('row.2.1', 'row.2.2')`
	expect(await sql`SELECT * FROM ${table}`).toMatchSnapshot()
})

test('rejects', async ()=>{

	const sql = sqlDb(':memory:');

	const table = sql('test_table');

	await expect(sql`invalid sql`).rejects.toThrowErrorMatchingSnapshot();
})
