import sqlTag from 'sql-execute-tag';
import sqlite3 from 'sqlite3';

const {
	OPEN_READONLY,
	OPEN_READWRITE,
	OPEN_CREATE,
	VERSION
} = sqlite3;

// Limit on the number of variables allowed in SQLITE statements (this was increased in v3.32.0)
export const MAX_VARIABLES = Number(VERSION.substr(2)) < 32 ? 999 : 32766;

export default function sqliteDb(file, options = {}) {
	if(options.verbose) {
		sqlite3.verbose();
	}

	const logError = options.logError || (()=>{});

	const db = new sqlite3.Database(file, options.mode);

	function execute(sql, values) {
		return new Promise((resolve, reject) => {
			db.all(sql, values, (e, r) => {
				if(e) {
					reject(e);
				} else {
					resolve(r);
				}
			});
		});
	}

	const sql = sqlTag(async (lits, vals) => {
		try {
			return await execute(lits.sql, vals);
		} catch(e) {
			logError(e, lits.sql, vals);
			throw e;
		}
	});

	sql.MAX_VARIABLES = MAX_VARIABLES;

	return sql;
}

export const mode = {
	OPEN_READONLY,
	OPEN_READWRITE,
	OPEN_CREATE
};

export { VERSION };
