# sqlite-execute-tag

Execute SQL statements against an sqlite3 database.

See package `sql-execute-tag` for the API.

```javascript
import sqlite3Db from 'sqlite-execute-tag';

const sql = slite3Db('/path/to/database/file.db');

const [row] = await sql`SELECT * FROM my_table LIMIT 1`;
const rows = await sql`SELECT * FROM my_table`;
await sql`INSERT INTO good_numbers (number) VALUES (${69}), (${420}), (${1312})`;
```
